# DAViCal Docker Container
Docker image for a complete [DAViCal](https://www.davical.org/) server (DAViCal
+ Apache2 + PostgreSQL) on Alpine Linux together with  a ldap integration.

### About DAViCal
[DAViCal](https://www.davical.org/) is a server for shared calendars. It
implements the [CalDAV protocol](https://wikipedia.org/wiki/CalDAV) and stores
calendars in the [iCalendar format](https://wikipedia.org/wiki/ICalendar).

List of supported clients: Mozilla Thunderbird/Lightning, Evolution, Mulberry, Chandler, iCal, ...

**Features**
>    - DAViCal is Free Software licensed under the General Public License.
>    - uses an SQL database for storage of event data
>    - supports backward-compatible access via WebDAV in read-only or read-write mode (not recommended)
>    - is committed to inter-operation with the widest possible CalDAV client software.
>
>DAViCal supports basic delegation of read/write access among calendar users, multiple users or clients reading and writing the same calendar entries over time, and scheduling of meetings with free/busy time displayed.
(*https://www.davical.org/*)

### Install


Production : 

- edit the `dav-env` file
- edit `davical.php`
- use your own postgres / set a volume for the dockerised postgres data persistence
- set the volume for ldap
- add a nginx reverse proxy on the host for :
    - ldap password UI
    - webdav endpoint / admin UI 


### Credits
Based on https://github.com/IridiumXOR/davical and https://hub.docker.com/r/oliveria/davical (no HTTPS, older PG and PHP versions).
